import React, { Component } from "react";
import about1 from "../assets/about1.png";
import about2 from "../assets/about2.png";
import about4 from "../assets/about4.png";
import about5 from "../assets/about5.png";
import about6 from "../assets/about6.png";
import about7 from "../assets/about7.png";
import about8 from "../assets/about8.png";
import about9 from "../assets/about9.png";
import about10 from "../assets/about10.png";

export default class About extends Component {
  render() {
    return (
      <div className="container">
        <div className=" aboutSection1 mt-5">
          <div className="aboutText">
            <h1 className="textH1">
              Строим и отделываем <br />
              дома
              <span> с 2011 года</span>
            </h1>
            <p className="textP1">
              Мы понимаем, что нашим заказчикам очень <br /> важно быть
              уверенными в компании, которая <br /> занимается созданием уюта и
              эффективности
              <br />в их домах.
              <br /> <br /> По предварительной записи,
              <br /> Вы можете посетить объект, который уже готов <br />
              или находится в процессе отделки <br /> <br /> По предварительной
              записи,
              <br /> Вы можете посетить объект, который уже готов <br /> или
              находится в процессе отделки
            </p>
            <div className="aboutBtn">
              <button className="aBtn1">РАСЧИТАТЬ СТОИМОСТЬ РАБОТ</button>
              <button className="aBtn2">Посетить готовый объект</button>
            </div>
          </div>
          <img src={about1} alt="man" />
        </div>
        <div className="aboutSection2 ">
          <h2>
            Команда <br /> профессионалов
          </h2>

          <div className="aBoxs">
            <div className="box">
              <img
                src={about2}
                alt="img"
                style={{
                  width: "310px",
                  height: "270px",
                }}
              />
              <div className="boxText">
                <h4>
                  Данченко Александр <br /> Анатольевич
                </h4>
                <p>Руководитель компании «Pinewood Homes»</p>
              </div>
            </div>
            <div className="box">
              <img
                src={about2}
                alt="img"
                style={{
                  width: "310px",
                  height: "270px",
                }}
              />
              <div className="boxText">
                <h4>
                  Данченко Александр <br /> Анатольевич
                </h4>
                <p>Руководитель компании «Pinewood Homes»</p>
              </div>
            </div>
            <div className="box">
              <img
                src={about2}
                alt="img"
                style={{
                  width: "310px",
                  height: "270px",
                }}
              />
              <div className="boxText">
                <h4>
                  Данченко Александр <br /> Анатольевич
                </h4>
                <p>Руководитель компании «Pinewood Homes»</p>
              </div>
            </div>
            <div className="box">
              <img
                src={about2}
                alt="img"
                style={{
                  width: "310px",
                  height: "270px",
                }}
              />
              <div className="boxText">
                <h4>
                  Данченко Александр <br /> Анатольевич
                </h4>
                <p>Руководитель компании «Pinewood Homes»</p>
              </div>
            </div>
            <div className="box">
              <img
                src={about2}
                alt="img"
                style={{
                  width: "310px",
                  height: "270px",
                }}
              />
              <div className="boxText">
                <h4>
                  Данченко Александр <br /> Анатольевич
                </h4>
                <p>Руководитель компании «Pinewood Homes»</p>
              </div>
            </div>
            <div className="box">
              <img
                src={about2}
                alt="img"
                style={{
                  width: "310px",
                  height: "270px",
                }}
              />
              <div className="boxText">
                <h4>
                  Данченко Александр <br /> Анатольевич
                </h4>
                <p>Руководитель компании «Pinewood Homes»</p>
              </div>
            </div>
          </div>
        </div>
        <div className="aboutSection3">
          <h3>
            Видеокейсы <br /> наших объектов
          </h3>
          <div className="sec">
            <img
              src={about4}
              alt="home"
              style={{
                width: "597px",
                height: "426px",
              }}
            />
            <img
              src={about4}
              alt="home"
              style={{
                width: "597px",
                height: "426px",
              }}
            />
          </div>
          <div className="secBtn3">
            <button>Загрузить еще</button>
          </div>
        </div>
        {/* <div className="aboutSection4">
          <h3>
            Галерея <br /> наших объектов
          </h3>
          <div className="card">
            <div className="aCard1">
              <img
                src={about5}
                style={{
                  width: "347px",
                  height: "464px",
                }}
              />
              <img
                src={about6}
                style={{
                  width: "347px",
                  height: "287px",
                }}
              />
            </div>
            <div className="aCard1">
              <img
                src={about7}
                style={{
                  width: "347px",
                  height: "287px",
                }}
              />
              <img
                src={about8}
                style={{
                  width: "347px",
                  height: "464px",
                }}
              />
            </div>
            <div className="aCard1">
              <img
                src={about9}
                style={{
                  width: "347px",
                  height: "464px",
                }}
              />
              <img
                src={about10}
                style={{
                  width: "347px",
                  height: "287px",
                }}
              />
            </div>
          </div>
        </div> */}
      </div>
    );
  }
}
