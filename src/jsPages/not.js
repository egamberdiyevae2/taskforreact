import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Not extends Component {
  render() {
    return (
      <div className="not-item">
        <h1>
          4 <span>0</span> 4
        </h1>
        <p>
          Упс... страница не найдена, вернитесь <br /> на главную страницу и
          продолжите поиск
        </p>
        <button className="not-button">
          <Link
            to="/"
            style={{
              textDecoration: "none",
              color: " #686656",
              fontFamily: "Roboto",
              fontSize: "18px",
              fontWeight: "400",
            }}
          >
            На главную
          </Link>
        </button>
      </div>
    );
  }
}
