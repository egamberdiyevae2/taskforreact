import React, { Component } from "react";
import iconPrice from "../assets/project-icon.svg";

export default class Price extends Component {
  render() {
    return (
      <div className="price-section">
        <h3>Фиксированные цены</h3>
        <div className="price-items">
          <div className="price-item">
            <h5>Двери и окна</h5>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <button>
              Оставить заявку <img src={iconPrice} alt="icon" />{" "}
            </button>
          </div>
          <div className="price-item">
            <h5>Двери и окна</h5>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <button>
              Оставить заявку <img src={iconPrice} alt="icon" />{" "}
            </button>
          </div>
        </div>
        <div className="price-items">
          <div className="price-item">
            <h5>Двери и окна</h5>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <button>
              Оставить заявку <img src={iconPrice} alt="icon" />{" "}
            </button>
          </div>
          <div className="price-item">
            <h5>Двери и окна</h5>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <button>
              Оставить заявку <img src={iconPrice} alt="icon" />{" "}
            </button>
          </div>
        </div>
        <div className="price-items">
          <div className="price-item">
            <h5>Двери и окна</h5>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <button>
              Оставить заявку <img src={iconPrice} alt="icon" />{" "}
            </button>
          </div>
          <div className="price-item">
            <h5>Двери и окна</h5>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <div className="price-list">
              <p>Подготовка дверного проема (обсада), шт.</p>
              <p>от 3 300,00 ₽</p>
            </div>
            <button>
              Оставить заявку <img src={iconPrice} alt="icon" />{" "}
            </button>
          </div>
        </div>
      </div>
    );
  }
}
