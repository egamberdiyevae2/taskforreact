import React, { Component } from "react";
import imageProduct from "../assets/project-image-1.png";
import iconProduct from "../assets/project-icon.svg";

export default class Project extends Component {
  render() {
    return (
      <div className="Project-section">
        <h3 className="project-title">Наши проекты</h3>
        <div className="Project-separate">
          <div className="project-item">
            <img className="image" src={imageProduct} alt="img" />
            <div className="project-description">
              <p style={{ marginBottom: "0" }}>КП Золотые сосны 290</p>
              <button>
                Смотреть проект <img src={iconProduct} alt="icon" />
              </button>
            </div>
          </div>
          <div className="project-item">
            <img className="image" src={imageProduct} alt="img" />
            <div className="project-description">
              <p style={{ marginBottom: "0" }}>КП Золотые сосны 290</p>
              <button>
                Смотреть проект <img src={iconProduct} alt="icon" />
              </button>
            </div>
          </div>
        </div>
        <div className="Project-separate">
          <div className="project-item">
            <img className="image" src={imageProduct} alt="img" />
            <div className="project-description">
              <p style={{ marginBottom: "0" }}>КП Золотые сосны 290</p>
              <button>
                Смотреть проект <img src={iconProduct} alt="icon" />
              </button>
            </div>
          </div>
          <div className="project-item">
            <img className="image" src={imageProduct} alt="img" />
            <div className="project-description">
              <p style={{ marginBottom: "0" }}>КП Золотые сосны 290</p>
              <button>
                Смотреть проект{" "}
                <img
                  style={{
                    width: "10px",
                    height: "7px",
                  }}
                  src={iconProduct}
                  alt="icon"
                />
              </button>
            </div>
          </div>
        </div>
        <button className="Final-Button">Загрузить еще</button>
      </div>
    );
  }
}
