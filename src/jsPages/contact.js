import React, { Component } from "react";
import Phone from "../assets/iphone.svg";
import Phoneicon from "../assets/Telephone.svg";
import Language from "../assets/language.svg";
import image3 from "../assets/Group 70.svg";
import mail from "../assets/Mail.svg";
import Map from "../assets/locate.svg";

export default class Contact extends Component {
  render() {
    return (
      <div className="contact-section">
        <h3>Контактные данные</h3>
        <div className="contact-items">
          <div className="contact-item">
            <img src={Phone} alt="icon" />
            <h6 style={{ marginTop: "65px" }}>Позвоните нам</h6>
            <p>Есть вопросы? Мы поможем!</p>
            <div className="small-item">
              <img src={Phoneicon} alt="icon" />
              <p>+7 777 777 77 77</p>
            </div>
          </div>
          <div className="contact-item" style={{ marginTop: "68px" }}>
            <img src={Language} alt="icon" />
            <h6>Напишите нам</h6>
            <p>
              Идеи? Предложения? <br /> Мы открыты для любых <br /> вопросов!
            </p>
            <div className="small-item">
              <img src={mail} alt="icon" />
              <p>info@pinewoodhomes.ru</p>
            </div>
          </div>
          <div className="contact-item" style={{ marginTop: "85px" }}>
            <img src={image3} alt="icon" />
            <h6 style={{ marginTop: "100px" }}>Закажите звонок</h6>
            <p>
              Мы перезвоним Вам <br />в ближайшее время!
            </p>
            {/* <div className="small-item">
              <img src={Phoneicon} alt="icon" />
              <p>+7 777 777 77 77</p>
            </div> */}
            <button>Заказать звонок</button>
          </div>
        </div>
        <div className="forLocation">
          <div className="locateDesc">
            <img src={Map} alt="map" />
            <p>
              Владимирская обл. г. Александров <br />
              ул. Ленина 13/7 БЦ Свечка, оф. 211
            </p>
          </div>
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d95872.4929262974!2d69.3239808!3d41.3302784!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1695363385801!5m2!1sen!2s"></iframe>
        </div>
      </div>
    );
  }
}
