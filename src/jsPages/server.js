import React, { Component } from "react";
import serverImg from "../assets/server-img.png";
import iconServer from "../assets/project-icon.svg";

export default class Server extends Component {
  render() {
    return (
      <div className="server-items">
        <h3>Наши услуги</h3>
        <div className="item-server">
          {/* <div className="item">
            <img
              src={serverImg}
              alt="img"
              style={{
                width: "315px",
                height: "261px",
              }}
            />
            <p>Шлифовка</p>
            <p>
              Подготовка дома к покраске <br />и увеличение срока службы каркаса
            </p>
            <button>
              Оставить заявку <img src={iconServer} alt="icon" />
            </button>
          </div> */}
          <div className="item">
            <img
              src={serverImg}
              alt="img"
              style={{
                width: "315px",
                height: "261px",
              }}
            />
            <p>Шлифовка</p>
            <p>
              Подготовка дома к покраске <br />и увеличение срока службы каркаса
            </p>
            <button>
              Оставить заявку <img src={iconServer} alt="icon" />
            </button>
          </div>
          <div className="item">
            <img
              src={serverImg}
              alt="img"
              style={{
                width: "315px",
                height: "261px",
              }}
            />
            <p>Шлифовка</p>
            <p>
              Подготовка дома к покраске <br />и увеличение срока службы каркаса
            </p>
            <button>
              Оставить заявку <img src={iconServer} alt="icon" />
            </button>
          </div>
          <div className="item">
            <img
              src={serverImg}
              alt="img"
              style={{
                width: "315px",
                height: "261px",
              }}
            />
            <p>Шлифовка</p>
            <p>
              Подготовка дома к покраске <br />и увеличение срока службы каркаса
            </p>
            <button>
              Оставить заявку <img src={iconServer} alt="icon" />
            </button>
          </div>
        </div>
        <div className="item-server">
          {/* <div className="item">
            <img
              src={serverImg}
              alt="img"
              style={{
                width: "315px",
                height: "261px",
              }}
            />
            <p>Шлифовка</p>
            <p>
              Подготовка дома к покраске <br />и увеличение срока службы каркаса
            </p>
            <button>
              Оставить заявку <img src={iconServer} alt="icon" />
            </button>
          </div> */}
          <div className="item">
            <img
              src={serverImg}
              alt="img"
              style={{
                width: "315px",
                height: "261px",
              }}
            />
            <p>Шлифовка</p>
            <p>
              Подготовка дома к покраске <br />и увеличение срока службы каркаса
            </p>
            <button>
              Оставить заявку <img src={iconServer} alt="icon" />
            </button>
          </div>
          <div className="item">
            <img
              src={serverImg}
              alt="img"
              style={{
                width: "315px",
                height: "261px",
              }}
            />
            <p>Шлифовка</p>
            <p>
              Подготовка дома к покраске <br />и увеличение срока службы каркаса
            </p>
            <button>
              Оставить заявку <img src={iconServer} alt="icon" />
            </button>
          </div>
          <div className="item">
            <img
              src={serverImg}
              alt="img"
              style={{
                width: "315px",
                height: "261px",
              }}
            />
            <p>Шлифовка</p>
            <p>
              Подготовка дома к покраске <br />и увеличение срока службы каркаса
            </p>
            <button>
              Оставить заявку <img src={iconServer} alt="icon" />
            </button>
          </div>
        </div>
        <button className="button-serverr">Загрузить еще</button>
      </div>
    );
  }
}
