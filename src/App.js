import React, { Component } from "react";
import { Routes, Route } from "react-router-dom";
import Home from "./jsPages/home";
import About from "./jsPages/about";
import Price from "./jsPages/price";
import Contact from "./jsPages/contact";
import Project from "./jsPages/project";
import Server from "./jsPages/server";
import Nav from "./component/Nav";
import Footer from "./component/Footer";
import Not from "./jsPages/not";
export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Nav />
        <main>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="project" element={<Project />} />
            <Route path="/server" element={<Server />} />
            <Route path="/price" element={<Price />} />
            <Route path="/about" element={<About />} />
            <Route path="/contact" element={<Contact />} />
            <Route path="/*" element={<Not />} />
          </Routes>
        </main>
        <Footer />
      </div>
    );
  }
}
